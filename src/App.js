import React, { Component,Img } from 'react';
import Typist from 'react-typist';
import '../node_modules/react-typist/dist/Typist.css'
import Partical from "../src/component/partical.js"
import ParticalCircle from "../src/component/particalcircle.js"
import './App.css';
import Parallax from 'react-springy-parallax';
import ReactDOM from 'react-dom';
import Footer from "../src/component/footer";
import {Animated} from "react-animated-css";
import ParticalStar from "../src/component/particalstar";
import img from "../src/icon/space.jpg"



const url = (name, wrap = false) => `${wrap ? 'url(' : ''}https://awv3node-homepage.surge.sh/build/assets/${name}.svg${wrap ? ')' : ''}`
class App extends Component {
    
    
    componentDidMount(){
        
    }
  render() {
    return ( 

        
        <Parallax ref='parallax' pages={3} >
        
        <Parallax.Layer offset={0} speed={1} factor={2} style={{ backgroundColor: '#191919' }} />
        <Parallax.Layer offset={1} speed={1} factor={2} style={{ backgroundColor: '#2e2e2e' }} />
        <Parallax.Layer offset={2} speed={1} style={{ backgroundColor: '#424242' }} 
         />
        <Parallax.Layer offset={2} speed={0.8} style={{ opacity: 0.1 }}>
          <img src={url('cloud')} style={{ display: 'block', width: '20%', marginLeft: '55%' }} />
          <img src={url('cloud')} style={{ display: 'block', width: '15%', marginLeft: '15%' }} />
        </Parallax.Layer>

        <Parallax.Layer offset={2} speed={0.5} style={{ opacity: 0.2 }}>
          <img src={url('cloud')} style={{ display: 'block', width: '15%', marginLeft: '70%' }} />
          <img src={url('satellite4')} style={{ display: 'block', width: '20%', marginLeft: '30%' }} />
        </Parallax.Layer>

        
         <div className="App">
         <div className="cont">
        <Parallax.Layer
            offset={0} 
            speed={1} 
            
            style={{ display: 'flex'}}
        > 


        <div className="header">
        
        <Animated animationIn="fadeInDown" isVisible={true}
            className="imge">
        <img className="img" src={require('./icon/react.png')} />
        </Animated>
        
        </div>




        <div className="partical">
        
        <Partical/>
        <div className="divyazisi">
    <Typist avgTypingDelay={150}
         className="yazi" 
         cursor={{ hideWhenDone: true }}>
        <Typist.Delay ms={1000} />
             Hakan YILMAZZ<br/>
             <Typist.Backspace count={7} delay={200} /> 
            YILMAZ
        </Typist>
        </div>
        
        </div>


        </Parallax.Layer>
        <Parallax.Layer
            offset={1} 
            speed={0.5} 
            onClick={() => this.parallax.scrollTo(2)}
            style={{ display: 'flex'}}
        > 


        
        <div className="hareket">
        <ParticalCircle/>
     <div className="skill"> YETENEKLER EN KISA ZAMANDA SİZLERLE :) </div>
                </div>
                </Parallax.Layer>
        </div>
        
       <Parallax.Layer
            offset={2} 
            speed={-0} 
            
            style={{ display: 'inherit'}}
            
    > 
        <div className="footer">
         {/*}   <ParticalStar/> */}
            <Footer/>
        </div>
        </Parallax.Layer>


      </div>
     
     </Parallax>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'))
export default App;
