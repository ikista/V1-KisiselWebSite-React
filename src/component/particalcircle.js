import React from 'react';
import Particles from 'react-particles-js';



class ParticalCircle extends React.Component {
  render() {
    return (
      <div>
      <Particles
         height="150vh"
    	params={{
	    "particles": {
	        "number": {
	            "value": 24,
	            "density": {
	                "enable": true,
	                "value_area": 800
	            }
	        },
	        "line_linked": {
	            "enable": false
	        },
	        "move": {
	            "speed": 3,
	            "out_mode": "out"
	        },
	        "shape": {
	            "type": 
	                
	                "circle"
	         },
	        "color": {
	            "value": "random"
	        },
	        "size": {
	            "value": 20,
	            "random": true,
	            "anim": {
	                "enable": true,
	                "speed": 8,
	                "size_min": 10,
	                "sync": false
	            }
	        }
	    },
	    "retina_detect": false
	}} />
             </div>
    );
  }
}


export default ParticalCircle;