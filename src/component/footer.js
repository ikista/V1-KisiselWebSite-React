import React from 'react';
import {Footer,FooterDropDownSection,FooterLinkList,FooterSection} from 'react-mdl';
import { SocialIcon } from 'react-social-icons';
import { FaPhone,FaMapMarker,FaEnvelope,FaReact,FaJs,FaJava } from 'react-icons/fa';
import '../../node_modules/react-typist/dist/Typist'
import Typist from 'react-typist';




class footer extends React.Component {
  render() {
    return (
        <div className="footer">
        <Footer className="alt" >
        <FooterSection type="mega">
    
        <FooterDropDownSection>
        <h6 className="Ability">YETENEKLER</h6>
            <FooterLinkList className="iletisim2">
            <div className="react">
                <a href="https://reactjs.org/" target="_blank"><FaReact/>  React</a>
                </div>
                <div className="react">
                <a href="https://facebook.github.io/react-native/"  target="_blank"><FaReact/>  React Native(Android / İOS)</a>
                </div>
                <div className="react">
                <a href="https://www.w3schools.com/js/" target="_blank"><FaJs/>  JavaScript</a>
                </div>
                <div className="react">
                <a href="http://www.oracle.com/technetwork/java/index-138747.html" target="_blank"><FaJava/>  Java</a>
                </div>
            </FooterLinkList>
        </FooterDropDownSection>
        <FooterDropDownSection>
        <h6 className="Ability">BEN KİMİM ?</h6>
            <FooterLinkList>
            <Typist 
            cursor={{ show: false }}
            avgTypingDelay={10}>
            <div className="edit">
            <p className="bilgi">
            BBC verilerine göre 5,541,919,379.uncu insan olarak 1993 yılında Balıkesirde doğdum. İlkokul ve Ortaokulu çeşitli illerde okuduktan sonra  Balıkesir Cumhuriyet Anadolu lisesinde lise eğitimimi tamamladım. Çanakkale Onsekiz Mart üniversitesinde Bilgisayar Mühendisliğini kazandım. Her ne kadar gelişen ve değişen dünyanın bütün yeniliklerini takip edemesem de dönem dönem ilgimi çeken gelişmeler olmakta son zamanların gözde konularından olan CriptoCoin piyasasını yakından takip etmekteyim.
            </p>
                  </div>
              </Typist>
            </FooterLinkList>
        </FooterDropDownSection>
        <FooterDropDownSection>
        <h6 className="Ability">EĞİTİM GEÇMİŞİ</h6>
            <FooterLinkList>
            <div className="plan">
                <p><b>1999-2007</b> -  İlkokul</p>
                <p><b>2007-2011</b> - Lise</p>
                <p><b>2012-2018</b> - Üniversite</p>
                </div>
                
            </FooterLinkList>
        </FooterDropDownSection>
        <h6 className="contact">İLETİŞİM</h6>
        <FooterDropDownSection className="iletisim">
            <FooterLinkList>
                <p className="number"><FaPhone /> : +90 (507) 512 39 49</p>
                <a href="https://www.google.com/maps/place/39%C2%B039'18.4%22N+27%C2%B051'03.7%22E/@39.655123,27.8504808,19z/data=!3m1!4b1!4m6!3m5!1s0x0:0x0!7e2!8m2!3d39.6551217!4d27.8510277" target="_blank"><FaMapMarker/> : Adnan Menderes Mah. 132. Sok. No:2/1 Balıkesir/Karesi</a>

                <div className="mail">
                <a href="mailto:hakanyilmazzz93@gmail.com" ><FaEnvelope/>  : hakanyilmazzz93@gmail.com</a>
                </div>
            </FooterLinkList>
        </FooterDropDownSection>
    </FooterSection>
    <FooterSection type="bottom" className="footer2">
        <FooterLinkList>
        <SocialIcon url="http://linkedin.com/in/hakan-yılmaz-2b2658b9" network="linkedin" color="#ff5a01"/>
        <SocialIcon url="https://www.instagram.com/h.ylmzzz/" network="instagram"  color="#ff5a01"/>
        <SocialIcon url="https://www.facebook.com/h.ylmzzz" network="facebook" color="#ff5a01"/>
        <SocialIcon url="https://twitter.com/realistpenguen"  network="twitter" color="#ff5a01"/>
        <SocialIcon url="https://github.com/ikista" network="github" color="#ff5a01"/>
        </FooterLinkList>
    </FooterSection>
</Footer>


  </div>
    );
  }
}


export default footer;